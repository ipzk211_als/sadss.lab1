const functions = require('./modules/functions')
const User = require('./modules/user')
const assert = require('assert')
const request = require('request')

let users = [
    {name: 'Test1', age: 12, email: 'test1@gmail.com' }, //0
    {name: 'Test2', age: -5, email: 'test2@gamil.com' }, //1
    {name: 'Test3', age: '18' }, //2
    {name: 'Test4', age: 4, email: 'test3@gamil.com' }, //3
    {name: 'Test5', age: 7, email: 'test4@gamil.com' }, //4
    {name: 'Test6', age: 9, email: 'test5gamil.com' }, //5
    {age: 10, email: '10@gamil.com' }, //6
    {name: ' ',  email: 'test123@gamil.com' }, //7
    {name: 'BASE', age: 'base', email:'base@gmail.com'} //8
]
it('adding two numbers', function(){
    var correctResult = 8
    var currentResult = functions.add(3,5)
    if(correctResult !== currentResult)
        throw new Error(`Your result is ${currentResult}, correct result might be ${correctResult}`)
})
describe('factorial', function(){
    it('F(0)',function(){
        assert.equal(functions.factorial(0),1)
    })
    it('F(-5)',function(){
        assert.equal(functions.factorial(-5),-120)
    })
    it('F(6)',function(){
        assert.equal(functions.factorial(6),720)
    })
})
describe('User',function(){
    for(let i = 0; i< users.length; i++){
        it(`User ${i} is testing`,function(done){
            var user = new User()
            user.name = users[i].name
            user.age = users[i].age
            user.email = users[i].email
            user.save(done)
        })
    }
})



describe('1-15 Tests', function () {
    let token
    let id
    it('1', (done) => {
        request({
            url: 'http://localhost:3000/users',
            method: 'POST', 
            mode: 'cors', 
            cache: 'no-cache', 
            credentials: 'same-origin', 
            headers: {
                'Content-Type': 'application/json',
                'name': 'Test',
                'surname':'sdad',
                'password': '12345',
                'age': '21'
            },
            redirect: 'follow', 
            referrerPolicy: 'no-referrer' 
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 403) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('2', (done) => {
        request({
            url: 'http://localhost:3000/users',
            method: 'POST',
            mode: 'cors', 
            cache: 'no-cache', 
            credentials: 'same-origin', 
            headers: {
                'Content-Type': 'application/json',
                'name': 'Test1',
                'surname':'SurTest',
                'email': 'Test1@gmail.com',
                'password': '11111111',
                'age': '21'
            },
            redirect: 'follow', 
            referrerPolicy: 'no-referrer' 
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 201) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('3', (done) => {
        request({
            url: 'http://localhost:3000/users',
            method: 'POST', 
            mode: 'cors', 
            cache: 'no-cache', 
            credentials: 'same-origin', 
            headers: {
                'Content-Type': 'application/json',
                'name': 'Test2',
                'surname':'SurTest2',
                'email': 'Test2@gmail.com',
                'password': '11111111',
                'age': '21'
            },
            redirect: 'follow', 
            referrerPolicy: 'no-referrer' 
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 201) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('4', (done) => {
        request({
            url: 'http://localhost:3000/users/login',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'email': 'Test1@gmail.com',
                'password': '11111111'
            }
        }, function (error, response) {
            console.log(response.statusCode)
            console.log(response.body)
            if (response.statusCode == 200) {
                token = JSON.parse(response.body).token
                done()
            } else {
                done(response.statusCode)
            }   
        })
    })
    it('5', (done) => {
        request({
            url: 'http://localhost:3000/tasks',
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'description': 'Task1',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 201) {
                id = JSON.parse(response.body)._id
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('6', (done) => {
        request({
            url: 'http://localhost:3000/tasks',
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'description': 'Task2',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 201) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('7', (done) => {
        request({
            url: 'http://localhost:3000/tasks',
            method: 'GET', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode)
            console.log(typeof(response.body))
            if (response.statusCode == 201) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('8', (done) => {
        request({
            url: 'http://localhost:3000/tasks/' + id,
            method: 'GET', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 200) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('9', (done) => {
        request({
            url: 'http://localhost:3000/users/logout',
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode + " logout success")
            if (response.statusCode == 200) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('10', (done) => {
        request({
            url: 'http://localhost:3000/users/login',
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'email': 'Test2@gmail.com',
                'password': '11111111'
            }
        }, function (error, response) {
            console.log(response.statusCode + " success")
            if (response.statusCode == 200) {
                token = JSON.parse(response.body).token
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('11', (done) => {
        request({
            url: 'http://localhost:3000/tasks',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'description': 'Task3',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 201) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('12', (done) => {
        request({
            url: 'http://localhost:3000/tasks',
            method: 'GET', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode)
            if (response.statusCode == 201) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('13', (done) => {
        request({
            url: 'http://localhost:3000/tasks/' + id,
            method: 'GET', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode + " Not Found")
            if (response.statusCode == 404) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('14', (done) => {
        request({
            url: 'http://localhost:3000/users/logout',
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode + " logout success")
            if (response.statusCode == 200) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
    it('15', (done) => {
        request({
            url: 'http://localhost:3000/tasks/' + id,
            method: 'GET', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }, function (error, response) {
            console.log(response.statusCode + ' Authorization error')
            if (response.statusCode == 401) {
                done()
            } else {
                done(response.statusCode)
            }
        })
    })
})